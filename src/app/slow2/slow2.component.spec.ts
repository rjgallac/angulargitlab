import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { Slow2Component } from './slow2.component';
import { CUSTOM_ELEMENTS_SCHEMA, NO_ERRORS_SCHEMA } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';

describe('Slow2Component', () => {
  let component: Slow2Component;
  let fixture: ComponentFixture<Slow2Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [Slow2Component],
      schemas: [CUSTOM_ELEMENTS_SCHEMA, NO_ERRORS_SCHEMA],
      imports: [HttpClientModule],
      providers: [HttpClientModule]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Slow2Component);
    component = fixture.componentInstance;
    component.message = 'test';
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

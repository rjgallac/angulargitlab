import { Component, OnInit, Input } from '@angular/core';
import { SharedService } from '../shared.service';

@Component({
  selector: 'app-slow2',
  templateUrl: './slow2.component.html',
  styleUrls: ['./slow2.component.css']
})
export class Slow2Component implements OnInit {
  @Input() message: string;

  get data() {
    if (this.sharedService.getData()) {
      return this.sharedService.getData().message;
    }
    return 'loading';
  }

  constructor(private sharedService: SharedService) {}

  ngOnInit() {
    this.sharedService.myMethod$.subscribe((data) => {
      // this.data = data; // And he have data here too!
      console.log('FOUND DATA', data);
  }
);
  }
}

import { Component, OnInit } from '@angular/core';
import { TodoService } from './todo.service';
import { Todo } from './Todo';
import { SlowService } from './slow.service';
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  todos: Todo[];
  title = 'front';
  message = '';

  constructor(private todoSerivce: TodoService, private slowService: SlowService) {}
  ngOnInit() {
    this.todoSerivce.getTodos().subscribe((res: Todo[]) => {
      this.todos = res;
    });

    this.slowService.get().subscribe((res: string) => {
      this.message = res;
    });
  }
}

import { TestBed, async, ComponentFixture } from '@angular/core/testing';
import { AppComponent } from './app.component';
import { HttpClientModule } from '@angular/common/http';
import { By } from '@angular/platform-browser';
import { TodoService } from './todo.service';
import { of } from 'rxjs';
import { DebugElement } from '@angular/core';
import { CUSTOM_ELEMENTS_SCHEMA, NO_ERRORS_SCHEMA } from '@angular/core';

describe('AppComponent', () => {
  let fixture: ComponentFixture<AppComponent>;
  let todoService: TodoService;
  let debugElement: DebugElement;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [AppComponent],
      imports: [HttpClientModule],
      providers: [HttpClientModule, TodoService],
      schemas: [CUSTOM_ELEMENTS_SCHEMA, NO_ERRORS_SCHEMA]
    }).compileComponents();
    fixture = TestBed.createComponent(AppComponent);
    debugElement = fixture.debugElement;
    todoService = debugElement.injector.get(TodoService);
  }));

  it('should create the app', () => {
    const app = fixture.debugElement.componentInstance;
    expect(app).toBeTruthy();
  });

  it(`should have as title 'front'`, () => {
    const app = fixture.debugElement.componentInstance;
    expect(app.title).toEqual('front');
  });

  it('should render title in a h1 tag', () => {
    fixture.detectChanges();
    expect(debugElement.query(By.css('h1')).nativeElement.innerText).toContain('Welcome to front!');
  });

  it('check li', () => {
    spyOn(todoService, 'getTodos').and.returnValue(of([{ todo: 'todo' }]));
    fixture.detectChanges();
    expect(debugElement.query(By.css('ul>li')).nativeElement.innerText).toBe('- todo');
    expect(debugElement.query(By.css('h1')).nativeElement.innerText).toContain('Welcome to front!');
  });

  it('check two lis', () => {
    spyOn(todoService, 'getTodos').and.returnValue(of([{ todo: 'todo' }, { todo: 'todo2' }]));
    fixture.detectChanges();
    expect(debugElement.queryAll(By.css('li')).length).toBe(2);
    expect(debugElement.queryAll(By.css('li'))[1].nativeElement.innerText).toBe('- todo2');
  });
});

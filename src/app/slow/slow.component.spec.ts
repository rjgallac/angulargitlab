import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SlowComponent } from './slow.component';
import { HttpClientModule } from '@angular/common/http';

import { CUSTOM_ELEMENTS_SCHEMA, NO_ERRORS_SCHEMA } from '@angular/core';

describe('SlowComponent', () => {
  let component: SlowComponent;
  let fixture: ComponentFixture<SlowComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [SlowComponent],
      schemas: [CUSTOM_ELEMENTS_SCHEMA, NO_ERRORS_SCHEMA],
      imports: [HttpClientModule],
      providers: [HttpClientModule]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SlowComponent);
    component = fixture.componentInstance;
    component.message = 'asdf';
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

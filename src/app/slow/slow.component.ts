import { Component, OnInit, Input } from '@angular/core';
import { SharedService } from '../shared.service';

@Component({
  selector: 'app-slow',
  templateUrl: './slow.component.html',
  styleUrls: ['./slow.component.css']
})
export class SlowComponent implements OnInit {
  @Input() message: string;

  private sharedData;

  get data() {
    if (this.sharedData) {
      return this.sharedData.message;
    }
    return 'loading';
  }

  constructor(private sharedService: SharedService) {}

  ngOnInit() {
    this.sharedService.get().subscribe(res => {
      this.sharedData = res;
      this.sharedService.setData(res);
      this.sharedService.myMethod(res);

    });
  }
}

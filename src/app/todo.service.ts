import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class TodoService {
  constructor(private http: HttpClient) {
    console.log(environment);
  }

  getTodos() {
    console.log(environment.apiUrl + 'todo');
    return this.http.get(environment.apiUrl + 'todo');
  }
}

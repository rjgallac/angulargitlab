import { TestBed } from '@angular/core/testing';

import { SlowService } from './slow.service';
import { HttpClientModule } from '@angular/common/http';

describe('SlowService', () => {
  beforeEach(() => TestBed.configureTestingModule({
    imports: [HttpClientModule],
    providers: [HttpClientModule]
  }));

  it('should be created', () => {
    const service: SlowService = TestBed.get(SlowService);
    expect(service).toBeTruthy();
  });
});

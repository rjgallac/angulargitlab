import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { HttpClientModule } from '@angular/common/http';
import { SlowComponent } from './slow/slow.component';
import { Slow2Component } from './slow2/slow2.component';
import { MyFormComponent } from './my-form/my-form.component';

import { FormsModule, ReactiveFormsModule } from '@angular/forms';

@NgModule({
  declarations: [AppComponent, SlowComponent, Slow2Component, MyFormComponent],
  imports: [BrowserModule, HttpClientModule, FormsModule, ReactiveFormsModule],
  providers: [HttpClientModule],
  bootstrap: [AppComponent]
})
export class AppModule {}
